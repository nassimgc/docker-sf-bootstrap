# Docker Symfony Bootstrap
## Configuration BDD
Créer à la racine du prochain un fichier `.env` et créer les variables d'environnement suivant :
- ```bash
            MYSQL_ROOT_PASSWORD=rootPassword
            MYSQL_DATABASE=votreBDD
            MYSQL_USER=votreUser
            MYSQL_PASSWORD=votreMdpUser
    ```
## Configuration Symfony
Ne pas oublier de configurer la base de données qui sera à utiliser dans le fichier  `./projet/.env` :

```bash
DATABASE_URL="mysql://app:!ChangeMe!@db_symfony:3306/app?serverVersion=8.0.32&charset=utf8mb4"
```
